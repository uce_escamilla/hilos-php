# hilos-php

Ejemplo de manejo de hilos en php

Ejemplo basado en [Forking de procesos en php - Francisco Ugalde](https://www.franciscougalde.com/2018/02/19/desde-cero-forking-de-procesos-en-php/)

## Cómo funciona

Tendremos instancias del proceso padre, el cual dará nuevos PID a los procesos hijo.

De los métodos de forking de PHP, estos son los dos que estarémos usando en nuestro ejemplo:

* pcntl_fork: Nos permite lanzar hijos de un proceso padre, devolviendo el pid del hijo lanzado. (pcntl_fork en PHP).

* pcntl_waitpid: Nos permite poner al padre en espera de que un hijo suyo termine su ejecución. El parámetro -1 indica que queda a la espera de que algún hijo termine, el primero que lo haga. (pcntl_waitpid en PHP).

* posix_kill: Mata (Kill -9 pid) el proceso marcado por su pid (process id).

La clase Task sobrescribe los siguientes métodos:

* initialize(): Es el primer método que se ejecuta al ejecutar la tarea. Imaginemos que aquí podemos implementar el proceso de seleccionar desde la base de datos los dispositivos a los que debemos enviar una notificación push.

* process(): Este método es el que lleva el proceso o lógica de la tarea. Podemos decir que aquí pondríamos el proceso de enviar la notificación a cada dispositivo. Si todo sale bien, disparamos el método onSuccess(), de lo contrario,
onFailure().

* onSuccess(): Este método se ejecuta cuando todo sale bien, puede servir para llevar un log de envíos de notificaciones enviadas.

* onFailure(): Este método se ejecuta cuando algo no ha salido bien, puede servir para llevar un log de los errores sucedidos y poder monitorizar lo que está pasando.
