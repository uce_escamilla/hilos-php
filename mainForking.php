<?php
 
require_once "managerHilos.php";
require_once "baseTask.php";
require_once "task.php";
 
$maxThreads = 5;
$pushNotifications = 30;
echo 'Ejemplo de forking de procesos con PHP con un máximo de ' . $maxThreads . ' hilos' . PHP_EOL . PHP_EOL;
$exampleTask = new Hilos\Task\Task();
$multithreadManager = new Hilos\ThreadManager();
 
$cpt = 0;
while (++$cpt <= $pushNotifications)
{
    $multithreadManager->start($exampleTask);
}